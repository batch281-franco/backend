const express = require("express");
const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.get("/", (req, res) => {
res.send("Welcome to the home page"); });

let users = [
	{
		"username":"john",
		"password":"johndoe"
	}
];

app.get("/users", (req, res) => {
res.send(users); });

app.delete("/delete-user", (req, res) => {
	let message;
	for(let i = 0; i<users.length; i++){
		if (req.body.username == users[i].username){
			users.splice(req.body);
			message = `Users ${req.body.username}'s has been deleted`;
			break;
		} else {
			message = "User is out of this world."; }}
	res.send(message); console.log(users) })


if(require.main === module){ app.listen(port, () => console.log(`Server is
running at port ${port}`)) }

module.exports = app;

	