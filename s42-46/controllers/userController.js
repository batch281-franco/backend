const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		// The find method returns a record if a match is found
		if(result.length > 0){
			return true;

		// No duplicate emails found
		// The user is not registered in the database
		} else {
			return false;
		};
	});
};



//	User registration 

module.exports.registerUser = (reqBody) => {

	// Creates a variable named "newUser" and insantiates a new "User" object using the Mondoose model
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {

		// User registration failed
		if(error){
			return false;

		// User registration successful
		} else {
			return true;
		}
	})

};

// User authentication

module.exports.loginUser = (reqBody) => {
	// the findOne method returns the first record in the collection that matches the search criteria
	// We use findOne method instead of find method which returns all records that match the criteria
	return User.findOne({email : reqBody.email}).then(result => {

		// User does not exist
		if(result == null){
			return false

		// User exists
		} else {
			// Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			// If the password matches
			if(isPasswordCorrect){
				// Generate an access token
				// Uses the createAccessToken method defined in the auth.js file
				return { access: auth.createAccessToken(result)}

			// Passwords do not match
			} else {
				return false;
			}
		}
	})
};


//	Non-admin User checkout (Create Order) 

module.exports.createOrder=async(req,res)=>{
    const userData=auth.decode(req.headers.authorization)
    //const orderId=req.body.orderId
    const productId=req.params.productId
    const productName=req.body.productName
  /*  const {quantity}=req.body*/
    
    if(!userData.isAdmin){
    	
        const product=await Product.findById(productId)
      /*  let totalAmount=quantity*product.productPrice*/
        product.userOrders.push({userId:userData.id});

        const user = await User.findById(userData.id)

        user.orderedProduct.push({
        	products:{
        		productId: product._id,
        		productName:product.productName
  /*              quantity  */       
        	},
 /*       	totalAmount	*/
        });
        product.save()
        user.save()
        return true;

        }else{
            return false;
        }
}

/*module.exports.createOrder = async (data) => {
	console.log(data)

	// Using the "await" keyword will allow the createOrder method to complete updating the user before returing a response back
	let isUserUpdated = await User.findById(data.userData.id).then(user => {

		// Adds the productId in the users createOrderment array
		user.orderedProduct.push({
			products:{
        		productId: data._id,
        		productName:data.productName       
        	}
		});

		// Saves the updated user information in the database
		return user.save().then((user, error) => {
			if(error){
				return false
			}else {
				return true;
			}
		});
	});

	let isProductUpdated = await Product.findById(data.productId).then(product => {
		// Adds the userId in the course's enrollees array
		product.userOrders.push({userId : data.userData.id});

		return product.save().then((product, error) => {
			if(error){
				return false;
			} else {
				return true;
			};
		});

	});

	if(isUserUpdated && isProductUpdated){
		return true;
	// User enrollment failure
	} else {
		return false
	};
}*/


//	Retrieve User Details:(user require login)

module.exports.userDetails = (data)=>{
	return User.findById(data.userId).then(result=>{
		//_id:  0 
		return result;
	})
};

//	Set user as admin (Admin only)

module.exports.updateRole = (reqParams, reqBody)=>{

	let updateAdmin={
		isAdmin:reqBody.isAdmin
		
	};
		return User.findByIdAndUpdate(reqParams.userId, updateAdmin).then((user, error)=>{
			if(error){
				return (`Course not updated, please signIn`);
			}else{

				return (`User Id (${reqParams.userId}) is Updated from IsAdmin to ${updateAdmin.isAdmin}`);
			}
	})
};


//Retrieve All Orders (admin only)


module.exports.getAllOrders=async(req,res)=>{
    const userData=auth.decode(req.headers.authorization)
    const data = req.body.userId
    const productId=req.body.productId
    const {quantity}=req.body
    const productName=req.body.productName
    const productPrice=req.body.productPrice
    if(userData.isAdmin=true){
        const product=await Product.findById(productId)
        let totalAmount=quantity*productPrice
        const user = await User//.findById(userData.id)
        return user.find({
        	orderedProduct:{
        		products:{
                		productId: productId,
                		productName:productName,
                        quantity         
               	},
        	}               		
    	});

      //  if(length>0){
 		  return user
 	}
       // }
}

module.exports.myOrders = (data)=>{

	return User.findById(data.userId).then((user,error)=>{
		if(error)
		{
			return false;
		}else
		{
			return user;
		}

	})
}



module.exports.allUsers=(data)=>{
	return User.find({}).then((user,error)=>{
		if(error)
		{
			return false;
		}else
		{
			return user;
		}

	})
};