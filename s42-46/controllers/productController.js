const Product = require("../models/Product");
const User=require('../models/User')
const bcrypt = require("bcrypt");
const auth=require('../auth.js');

//	Create Product (Admin only)

module.exports.createProduct=(data)=>{
	let newProduct=new Product({
		productName:data.product.productName,
		productDescription:data.product.productDescription,
		productPrice:data.product.productPrice
	});
	return newProduct.save().then((product, error) => {
		if(error){
			return false;

		}else {
			return true;
		};
	})
};

//	Retrieve all products 

module.exports.getAllProducts=()=>{
	return Product.find({}).then(result=>{
		return result;
		});
};

//	Retrieve all active products

module.exports.getAllActive=()=>{
	return Product.find({isActive:true}).then(result=>{
		return result
	})
};

//	Retrieve single product 

module.exports.getProduct = (reqParams) =>{
	return Product.findById(reqParams.productId).then(result=>{
		return result;
	});
};

//	Update Product information (Admin only) 

module.exports.updateProduct = (reqParams, reqBody)=>{
	let updateProduct={
		productName:reqBody.productName,
		productDescription:reqBody.productDescription,
		productPrice:reqBody.productPrice,
		isActive:reqBody.isActive
	};
		return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, error)=>{
			if(error){
				return (`Product not updated, please signIn`);
			}else{
				return (`${updateProduct.productName} successfully updated`);
			}
	})
};

//	● Archive Product (Admin only) 

module.exports.archieveProduct = (reqParams)=>{
	let updateActiveField ={
		isActive:false
	};
	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error)=>{
		if (error){
			return (`Product not archieve`);

		}else{
			return (`Product archieved successfully`);
		}
	});
};

//	● Activate Product (Admin only) 

module.exports.activateProduct = (reqParams)=>{
	let updateActiveField ={
		isActive:true
	};
	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error)=>{
		if (error){
			return (`Product not Activate`);

		}else{
			return (`Product Activate successfully`);
		}
	});
};



