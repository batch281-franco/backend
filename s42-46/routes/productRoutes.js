const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const userController = require("../controllers/userController");
const auth = require("../auth");

//	Create Product (Admin only)

router.post("/createProduct",(req,res)=>{
let data={
		product:req.body,
		isAdmin:auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin==true){
		productController.createProduct(data).then(resultFromController=> res.send(resultFromController));
	}else{
		res.send(`Access Denied... Admin required`)
	}
	
})

//	Retrieve all products 

router.get("/all",(req,res)=>{
	productController.getAllProducts().then(resultFromController=>res.send(resultFromController));
});

//	Retrieve all active products

router.get("/getAllActive",(req,res)=>{
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});

//	Retrieve single product 

router.get("/:productId",(req,res)=>{
	//console.log(req.params.courseId);
	productController.getProduct(req.params).then(resultFromController=> res.send(resultFromController));
});

//	Update Product information (Admin only) 

router.put("/:productId",(req,res)=>{
	const data={
		isAdmin:auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin==true){
		productController.updateProduct(req.params, req.body).then(resultFromController=>res.send(resultFromController));
	}else{
		res.send(`Access Denied`);
	}
});



//	● Archive Product (Admin only) 

router.put("/archive/:productId", auth.verify,(req,res)=>{
	const data={
		isAdmin:auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin==true){
		productController.archieveProduct(req.params).then(resultFromController=>res.send(resultFromController));
	}else{
		res.send(`Access Denied`);
	}
});

//	● Activate Product (Admin only) 

router.put("/activate/:productId",(req,res)=>{
	const data={
		isAdmin:auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin==true){
		productController.activateProduct(req.params, req.body).then(resultFromController=>res.send(resultFromController));
	}else{
		res.send(`Access Denied`);
	}
});




module.exports = router;



