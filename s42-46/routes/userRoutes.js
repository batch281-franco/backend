const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const productController = require("../controllers/productController");
const auth = require("../auth");


router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});


//	User registration: (any one can register)

router.post("/register", (req,res)=>{
	userController.registerUser(req.body).then(resultFromController=> res.send(resultFromController));
});

//	User authentication 

router.post("/login", (req, res)=>{
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

//	Non-admin User checkout (Create Order) 

router.post("/createOrder/:productId", auth.verify,(req,res)=>{
	userController.createOrder(req,res).then(resultFromController=> res.send(resultFromController));
});


/*router.post("/createOrder", auth.verify, (req, res) => {
	let data = {
		userData : auth.decode(req.headers.authorization),
		productId : req.body.productId
	}
	if(!data.isAdmin){
	userController.createOrder(data).then(resultFromController => res.send(resultFromController))
	}else{
		res.send(`You are not authorize`);
	}
})*/

//	Retrieve User Details:(user require login)

router.get("/details", auth.verify,(req,res)=>{
	const userData=auth.decode(req.headers.authorization);
	userController.userDetails({userId:userData.id}).then(resultFromController=>res.send(resultFromController));
});


//	Set user as admin (Admin only)

router.put("/:userId", auth.verify,(req,res)=>{
	const data={
		isAdmin:auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin==true){
		userController.updateRole(req.params, req.body).then(resultFromController=>res.send(resultFromController));
	}else{
		res.send(`You are not authorize`);
	}
});

//Retrieve All Orders (admin only)

router.get("/orders",(req,res)=>{
	userController.getAllOrders(req,res).then(resultFromController=>res.send(resultFromController));
});

//	Retrieve authenticated user’s orders

router.get("/myOrders", auth.verify,(req,res)=>{
	const userData=auth.decode(req.headers.authorization);
	if(!userData.isAdmin){
	userController.myOrders({userId:userData.id}).then(resultFromController=>res.send(resultFromController));
	}else{
		res.send(`Admin not allowed`);
	}
});


router.put("/:allusers", auth.verify,(req,res)=>{
	const data={
		isAdmin:auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin==true){
		userController.updateRole(data).then(resultFromController=>res.send(resultFromController));
	}else{
		res.send(`You are not authorize`);
	}
});

router.get("/allusers",(req,res)=>{
	const data={
		isAdmin:auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin==true){
		userController.allUsers(data).then(resultFromController=>res.send(resultFromController));
	}else{
		res.send(`You are not authorize`);
	}
});


module.exports = router;
