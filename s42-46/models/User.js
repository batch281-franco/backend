const mongoose = require("mongoose");
const userSchema = new mongoose.Schema(
{
	firstName :
	{
		type : String,
		required : [true, "firstName is required"]
	},
	lastName :
	{
		type : String,
		required : [true, "lastName is required"]
	},
	email :
	{
		type : String,
		required : [true, "email is required"]
	},
	mobileNo :
	{
		type : Number,

	},
	password : 
	{
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : 
	{
		type : Boolean,
		default : false,
	},
	orderedProduct:[
	{
		products:[
		{
			productId:
			{
		    	type:String,
		   		require:[true,'productId is required']
			},
			productName:
			{
			    type:String,
			    require:[true,'productName Id is required']
			},
			quantity:
			{
		   		type: Number
			}
		}],
		totalAmount:{
		    type: Number ,
		},
		purchasedOn:{
			type: Date, 
			default: new Date()
		}	
    }]
});

module.exports = mongoose.model("User", userSchema);
