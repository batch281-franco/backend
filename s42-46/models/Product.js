const mongoose=require('mongoose')

const productSchema=new mongoose.Schema(
{
    productName: 
        {
            type: String, 
            required:[true, "productName is required"]
        },
    productDescription: 
        {
            type: String,
            required :[true,"productDescription is required."]
        },
    productPrice: 
        {
            type: Number, 
        },
    isActive: 
        {
            type: Boolean, 
            default: true
        },
    createdOn: 
        {
            type: Date, 
            default: new Date()
        },
    userOrders: [
        {
            userId: 
            {
                type: String, 
                required:[true, 'User ID is required']
            },
            orderId: 
            {
                type: String
            }
        }
    ]
});

module.exports=mongoose.model('Product',productSchema)